package com.adamkorzeniak.utils.logging.test.utils;

import com.adamkorzeniak.utils.logging.utils.RequestUtils;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RequestUtilsTest {

    @Test
    void GetRequestURl_QueryStringMissing_ReturnsUrl() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getMethod()).thenReturn(TestData.HTTP_POST_METHOD);
        when(request.getRequestURI()).thenReturn(TestData.SIMPLE_PATH);
        when(request.getQueryString()).thenReturn(null);

        String signature = RequestUtils.getRequestSignature(request);

        assertThat(signature).isEqualTo(TestData.SIMPLE_REQUEST_SIGNATURE);
    }

    @Test
    void GetRequestURl_QueryStringIncluded_ReturnsUrl() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getMethod()).thenReturn(TestData.HTTP_GET_METHOD);
        when(request.getRequestURI()).thenReturn(TestData.COMPLEX_PATH);
        when(request.getQueryString()).thenReturn(TestData.COMPLEX_PATH_QUERY);

        String signature = RequestUtils.getRequestSignature(request);

        assertThat(signature).isEqualTo(TestData.COMPLEX_REQUEST_SIGNATURE);
    }

}
