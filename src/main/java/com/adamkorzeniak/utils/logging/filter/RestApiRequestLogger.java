package com.adamkorzeniak.utils.logging.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import org.slf4j.MDC;

@Slf4j
public class RestApiRequestLogger extends OncePerRequestFilter {

    private static final String CORRELATION_ID_KEY = "correlationId";

    private static String getContentAsString(byte[] buf, String charsetName) {
        if (buf == null || buf.length == 0) return "";
        int length = Math.min(buf.length, 10000);
        try {
            return new String(buf, 0, length, charsetName);
        } catch (UnsupportedEncodingException ex) {
            return "Unsupported Encoding";
        }
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String correlationId = UUID.randomUUID().toString();
        MDC.put(CORRELATION_ID_KEY, correlationId);

        String method = request.getMethod();
        String url = request.getRequestURI();

        String queryString = request.getQueryString();
        if (queryString != null) {
            url += "?" + queryString;
        }

        var beforeData = new BeforeRequestData(method, url);
        log.debug("Request received: {}", beforeData);

        ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(response);

        filterChain.doFilter(wrappedRequest, wrappedResponse);

        MDC.clear();

        String requestBody = getContentAsString(wrappedRequest.getContentAsByteArray(), request.getCharacterEncoding());
        int status = response.getStatus();
        String responseBody = getContentAsString(wrappedResponse.getContentAsByteArray(), response.getCharacterEncoding());

        var afterResponse = new AfterResponseData(beforeData, requestBody, status, responseBody);
        log.debug("Response returned: {}", afterResponse);
        wrappedResponse.copyBodyToResponse();
    }
}
