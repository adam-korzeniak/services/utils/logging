package com.adamkorzeniak.utils.logging.test.utils;

public class TestData {
    public static final String HTTP_GET_METHOD = "GET";
    public static final String HTTP_POST_METHOD = "POST";

    public static final String TEST_PATH = "/test";
    public static final String TEST_PATH_QUERY = "test=true";

    public static final String SIMPLE_PATH = "/simple";
    public static final String SIMPLE_REQUEST_SIGNATURE = "POST: /simple";

    public static final String COMPLEX_PATH = "/complex";
    public static final String COMPLEX_PATH_QUERY = "name=adam&skill=expert";
    public static final String COMPLEX_REQUEST_SIGNATURE = "GET: /complex?name=adam&skill=expert";

    public static final String TEST_KEY = "testKey";
    public static final String TEST_VALUE = "testValue";

    public static final String MDC_CORRELATION_ID_KEY = "correlationId";
    public static final String MDC_REQUEST_SIGNATURE_KEY = "requestSignature";

    public static final String TEST_METHOD_SIGNATURE = "Test Signature String";
}
