package com.adamkorzeniak.utils.logging.test.utils;

public class Messages {
    public static final String ENTERING_METHOD_MESSAGE = String.format("Method entered: %s", TestData.TEST_METHOD_SIGNATURE);
    public static final String EXITING_METHOD_MESSAGE = String.format("Method exited: %s", TestData.TEST_METHOD_SIGNATURE);

    public static final String REQUEST_WITHOUT_QUERY_RECEIVED_MESSAGE = "Request received for GET: /test";
    public static final String REQUEST_RECEIVED_MESSAGE = "Request received for GET: /test?test=true";
    public static final String RESPONSE_RETURNED_MESSAGE = "Response returned for GET: /test?test=true";

}
