package com.adamkorzeniak.utils.logging.utils;

import lombok.Generated;
import lombok.experimental.UtilityClass;

import javax.servlet.http.HttpServletRequest;

@Generated
@UtilityClass
public class RequestUtils {

    public static String getRequestSignature(HttpServletRequest request) {
        String method = request.getMethod();
        String uri = request.getRequestURI();
        String query = "";
        if (request.getQueryString() != null) {
            query = "?" + request.getQueryString();
        }
        return String.format("%s: %s%s", method, uri, query);
    }
}
